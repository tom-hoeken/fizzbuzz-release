import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "fizzbuzz-release")
public class FizzBuzzRelease extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    /**
     * Measures whether the release is a numbered, fizz, buzz or fizzbuzzz release
     * (Very important).
     *
     * @throws MojoExecutionException
     * @throws MojoFailureException
     */
    public void execute() throws MojoExecutionException, MojoFailureException {
        final String[] numbers = project.getVersion()
                .replace("-SNAPSHOT", "").split("\\.");

        final int i = Integer.parseInt(numbers[0]);
        logFizzBuzz(i);
    }

    private void logFizzBuzz(final int number) {
        final StringBuilder builder = new StringBuilder();
        if (number % 3 == 0) {
            builder.append("Fizz");
        }
        if (number % 5 == 0) {
            builder.append("Buzz");
        }

        if (builder.length() == 0) {
            getLog().warn("No fizz buzz compatible major release detected");
        } else {
            getLog().info(builder.toString());
        }
    }
}
